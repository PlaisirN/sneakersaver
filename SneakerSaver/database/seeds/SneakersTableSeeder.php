<?php

use Illuminate\Database\Seeder;
use App\Sneaker;
use App\User;

class SneakersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $jordanBanned = new Sneaker();
        $jordanBanned->user_id = 1;
        $jordanBanned->title = 'Jordan 1 Banned';
        $jordanBanned->material = 'Leather';
        $jordanBanned->save();

        $adidasFlux = new Sneaker();
        $adidasFlux->user_id = 1;
        $adidasFlux->title = 'ZX Flux Weave 2.0';
        $adidasFlux->material = 'Suede';
        $adidasFlux->save();

        $asics = new Sneaker();
        $asics->user_id = 1;
        $asics->title = 'Gel Lyte 3 Puddle Pack';
        $asics->material = 'Canvas';
        $asics->save();

        $fp = new Sneaker();
        $fp->user_id = 1;
        $fp->title = 'Low Top Perforated';
        $fp->material = 'Nubuck';
        $fp->save();

        $nmd = new Sneaker();
        $nmd->user_id = 1;
        $nmd->title = 'NMD R1 PK';
        $nmd->material = 'Velour';
        $nmd->save();
    }
}
