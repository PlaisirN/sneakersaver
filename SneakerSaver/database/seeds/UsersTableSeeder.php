<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new User();
        $user->name = 'Plaisir Nzuzi';
        $user->email = 'plaisir@voorbeeld.com';
        $user->password = bcrypt('test123');
        $user->save();
    }
}
