<?php

namespace App\Http\Controllers;

use App\Sneaker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SneakerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::check()){
            $sneakers = Sneaker::where('user_id', Auth::id())->get();

            return view('sneakers.index', ['sneakers' => $sneakers]);
        }else{
            return redirect('/welcome');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Auth::check()) {
            return view('sneakers.create');
        }else{
            return redirect('/welcome');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $sneaker = new Sneaker();
        $data = $this->validate($request, [
            'title' => 'required',
            'material' => 'required',
        ]);

        $sneaker->saveSneakerToDB($data);
        return redirect('/home')->with('success', 'New sneaker added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Sneaker $sneaker)
    {
        //
        if(Auth::check()){
            $sneaker = Sneaker::where('id', $sneaker->id)->first();

            return view('sneakers.show', ['sneaker' => $sneaker]);
        }else{
            return redirect('welcome');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sneaker $sneaker)
    {
        //
        if(Auth::check()){
            $sneaker = Sneaker::where('id', $sneaker->id)->where('user_id', auth()->user()->id)->first();

            return view('sneakers.edit', ['sneaker' => $sneaker]);
        }else{
            return redirect('welcome');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sneaker $sneaker)
    {
        date_default_timezone_set('Europe/Amsterdam');
        //
        //Find sneaker with the same ID and update the title, description and price
        $i = 0;
        $sneakerUpdate = Sneaker::where('id', $sneaker->id)
            ->where('user_id', auth()->user()->id)
            ->update([
                'title' => $request->input('title'),
                'material' => $request->input('material'),
                'lastwornon' => $request->input('lastwornon'),
                'timesworn' => DB::raw('timesworn + 1')
            ]);

        //If it it successfully updated, redirect to route
        if($sneakerUpdate){
            return redirect()->route('sneakers.show', ['sneaker'=>$sneaker->id])->with('success', 'The sneaker page is updated!');
        }

        //If all fails, take us back to the page that brought us here
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(Auth::check()){
            $sneaker = Sneaker::find($id);
            $sneaker->delete();

            return redirect('/sneakers')->with('success', 'Sneaker successfully deleted.');
        }else{
            return redirect('welcome');
        }
    }
}
