<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gmopx\LaravelOWM\LaravelOWM;
use App\Sneaker;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sneakers = Sneaker::where('user_id', Auth::id())->get()->where('lastwornon', !'0');
        $lastSneakers = Sneaker::where('user_id', Auth::id())->orderBy('timesworn', 'desc')->get();

        $lowm = new LaravelOWM();
        $forecast = $lowm->getWeatherForecast('rotterdam');

        return view('home', ['weather' => $forecast, 'sneakers' => $sneakers, 'lastsneakers' => $lastSneakers]);
//        return view('home');
    }
}
