<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sneaker extends Model
{
    //
    protected $fillable = [
        'title',
        'material',
        'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

//    public function bid(){
//        return $this->hasMany('App\Bid');
//    }

    public function saveSneakerToDB($data){
        $this->user_id = auth()->user()->id;
        $this->title = $data['title'];
        $this->material = $data['material'];
        $this->save();
        return 1;
    }
}
