@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <form method="post" action="{{ route('sneakers.update', [$sneaker->id]) }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="_method" value="put">
                    <div class="form-group">
                        <label for="sneaker-title">Title</label>
                        <input type="text" class="form-control" id="sneaker-title" placeholder="Enter Title" required value="{{ $sneaker->title }}" name="title">
                    </div>
                    <div class="form-group">
                        <label for="sneaker-material">material</label>
                        <input type="text" class="form-control" id="sneaker-material" placeholder="Enter Material" required value="{{ $sneaker->material }}" name="material">
                        {{--<textarea class="form-control" id="sneaker-material" placeholder="Enter material" style="resize: vertical" name="material">--}}
                        {{--{{ $sneaker->material }}--}}
                    {{--</textarea>--}}
                    </div>
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                </form>
            </div>
        </div>
    </div>

@endsection
