@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <a href="{{ url('sneakers/create') }}"><button class="btn btn-primary pull-right">Add A Sneaker</button></a>
        </div>
        <div class="row">
            @foreach($sneakers as $sneaker)
                <div class="col-xs-6 shoeCard">
                    <div class="card" style="">
                        <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
                        <div class="card-body">
                            <h4 class="card-title">{{$sneaker->title}}</h4>
                            <h4 class="card-title">Material: {{$sneaker->material}}</h4>
                            <img src="img/SneakerSaver-Logo.png" alt="sneaker">
                            <a href="{{ url('sneakers/' . $sneaker->id) }}" class="btn btn-primary btn1">View More</a>
                            <form method="post" action="{{ route('sneakers.update', [$sneaker->id]) }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="put">
                                <input type="hidden" name="title" value="{{ $sneaker->title }}">
                                <input type="hidden" name="material" value="{{ $sneaker->material }}">
                                <input type="hidden" name="lastwornon" value="{{ date('d-m-Y H:i') }}">
                                <button type="submit" class="btn btn-primary btn1">Wear Now!</button>
                            </form>
                            @if($sneaker->lastwornon != '0')
                                <form method="post" action="{{ route('sneakers.update', [$sneaker->id]) }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="put">
                                    <input type="hidden" name="title" value="{{ $sneaker->title }}">
                                    <input type="hidden" name="material" value="{{ $sneaker->material }}">
                                    <input type="hidden" name="lastwornon" value="0">
                                    <button type="submit" class="btn btn-primary btn1">Take Off!</button>
                                </form>
                            @endif
                            <h5>Wearing since: {{ $sneaker->lastwornon }}</h5>
                            <h5>Worn {{ $sneaker->timesworn }} times.</h5>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
