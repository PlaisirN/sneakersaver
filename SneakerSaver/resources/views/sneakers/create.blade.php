@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <form method="post" action="{{ route('sneakers.store') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="sneaker-title">Title</label>
                        <input type="text" class="form-control" id="sneaker-title" placeholder="Enter Title" required value="" name="title">
                    </div>
                    <div class="form-group">
                        <label for="sneaker-material">Material</label>
                        <input type="text" class="form-control" id="sneaker-material" placeholder="Enter Material" required value="" name="material">
                    </div>
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                </form>
            </div>
        </div>
    </div>
@endsection
