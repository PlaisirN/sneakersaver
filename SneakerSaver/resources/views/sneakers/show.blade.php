@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="card" style="">
                    <!-- <img class="card-img-top" src="..." alt="Card image cap" src="https://dummyimage.com/600x400/000/fff"> -->
                    <div class="card-body">
                        <h4 class="card-title">{{$sneaker->title}}</h4>
                        <h4 class="card-title">{{$sneaker->material}}</h4>
                        <hr>
                        <h4>
                            @php

                                if($sneaker->lastwornon != "0")
                                {
                                    echo "Currently wearing shoe since " . $sneaker->lastwornon . ".";
                                }

                            @endphp
                        </h4>
                        <div class="bids">
                            <ul class="list-group">
                                {{--@foreach($sneaker->bid as $bid)--}}
                                    {{--<li class="list-group-item">--}}
                                        {{--<strong>{{ $bid->created_at->diffForHumans() }}:</strong> &nbsp;--}}
                                        {{--€{{ $bid->bid }} by: {{ $bid->user->name }}--}}
                                    {{--</li>--}}
                                {{--@endforeach--}}
                            </ul>
                        </div>

                        <hr>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="">
                    <h4>Actions</h4>
                    @if(auth()->user()->id == $sneaker->user_id)
                        <ol class="list-unstyled">
                            {{--<li><a href="{{ url('sneakers/' . $sneaker->id . '/edit') }}">Edit Post</a></li>--}}
                            <li><a href="{{ url('sneakers/create') }}">Add A Sneaker</a></li>
                            <li><a href="{{ url('sneakers') }}">View All Sneakers</a></li>
                            <br/>
                            <form action="{{action('SneakerController@destroy', $sneaker->id)}}" method="post">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger" type="submit">Delete This Sneaker</button>
                            </form>
                        </ol>
                    @else
                        <ol class="list-unstyled">
                            <li><a href="{{ url('sneakers/create') }}">Add A Sneaker</a></li>
                            <li><a href="{{ url('sneakers') }}">View All Sneakers</a></li>
                        </ol>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
