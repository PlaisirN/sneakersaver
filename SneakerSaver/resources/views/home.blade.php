@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            @php
                $i = 0;
                foreach ($weather as $weer) {
                                            $i++;
                    if($i == 1) {
                        echo "Temperature for first 3 hours " . $weer->temperature . "<br />";
                        echo "Summary: " . $weer->weather . "<br />";
                        if (strpos($weer->weather, 'rain') !== false) {
                            echo 'The weather is dirty! Spray in your shoes!<br />';
                        }
                        echo "---<br />\n";
                    }


                    if($i == 2){
                        continue;
                    }

                    if($i == 3){
                        echo "Temperature 6 hours later is " . $weer->temperature . "<br />";
                        echo "Summary: " . $weer->weather . "<br />";
                        echo "---<br />\n";
                        break;
                        }

                    //if($i == 3) break;
                }
            @endphp

            @foreach($sneakers as $sneaker)

                <h4>Currently wearing:</h4>
                <img src="img/SneakerSaver-Logo.png" alt="Sneaker">
                <p>{{ $sneaker->title }} since {{ $sneaker->lastwornon }}</p>
                @php
                    $dateThen = strtotime($sneaker->lastwornon);
                    $dateLater = strtotime("+2 weeks", $dateThen);

                $current = strtotime('now');

                $percentage = (($current - $dateThen) / ($dateLater - $dateThen)) * 100;

                $realPercentage = 100 - $percentage;

                @endphp
                <p>Time until next spray: {{ date('d-m-Y', $dateLater) }}</p>
                <p>{{ round($realPercentage, 2) }}% protected.</p>
                @if($realPercentage < '20.00')
                <p>Almost time to spray them in again!</p>
                @endif

            @endforeach
            <form method="post" action="{{ route('sneakers.update', [$sneaker->id]) }}" style="margin-bottom:10px;">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="title" value="{{ $sneaker->title }}">
                <input type="hidden" name="material" value="{{ $sneaker->material }}">
                <input type="hidden" name="lastwornon" value="0">
                <button type="submit" class="btn btn-warning">Take Off!</button>
            </form>
            @php
            $j = 0;
            @endphp
            @foreach($lastsneakers as $ls)
                @php
                $j++;
                @endphp
                <div class="col-xs-4">
                    <p>{{ $ls->title }}</p>
                    <p>Times worn: {{ $ls->timesworn }}</p>
                </div>
            @endforeach
            <div class="col-xs-12">
                <a href="{{ url('sneakers/create') }}" style="margin-right:7px;"><button class="btn btn-success">Add A Sneaker</button></a>
                <a href="{{ url('sneakers') }}"><button class="btn btn-primary">View All Sneakers</button></a>
            </div>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
        </div>
    </div>
</div>
@endsection
